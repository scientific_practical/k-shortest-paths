#!/usr/bin/env python3

"""By executing the project, we will start to run Eppstein's Algorithm and Hershberger's Algorthim
in two seperate Threads.
A neighbourhood-Graph according to MAIN_GRAPH_PROPERTIES will be generated, if there is no main-graph in the `graphs` folder.

The graph will then be used by both Algorithms.

After both Algorithms have finished, the benchmark-results will be shown in the command line.
Also, a browser-tab will be openend, displaying the graph with the calculated shortest paths.
"""


from k_shortest_paths.__main__ import main

# graph properties, to generate the main-graph with.
# we assume the main graph to be a neighbourhood graph.
# Generation of neighbourhood graphs need 'n', 'm' and 'k' as parameters, 
# where n is the number of rows, m is the number of columns and k is the number of 
# shortest paths which should be pre-calculated.
# Therefore, this is a tuple (n, m, k)
MAIN_GRAPH_PROPERTIES = (20, 20, 100)


main(MAIN_GRAPH_PROPERTIES)