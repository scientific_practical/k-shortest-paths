#!/usr/bin/env python3
"""Run this script to plot the results of the benchmark tests in the statistics
folder after running the benchmark script.
"""
from k_shortest_paths.visualization.benchmark_plot import plot_neighborhood_statistics, plot_random_statistics

plot_neighborhood_statistics()
plot_random_statistics()
