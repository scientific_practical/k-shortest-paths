"""This File defines some utility-methods, which can be used by any Directed Graph."""

import networkx as nx
from functools import reduce
import k_shortest_paths.common.dijkstra as dijkstra
from k_shortest_paths.common.tree import Tree

def tail(e):
    """Returns the tail of the directed edge `e`
    """
    return e[0]


def head(e):
    """Returns the head of the directed edge `e`
    """
    return e[1]


def length(G: nx.DiGraph, path: [] = [], weight="weight"):
    """
    Returns the length of a path in a given graph,
    such that the `weight`-Attribute of each edge is accumulated.
    We assume, that the path `path` is actually a path in `G`.
    """
    #weights = (G[path[i]][path[i + 1]][weight] for i in range(len(path) - 1))
    #return reduce((lambda w1, w2: w1 + w2), weights, 0)

    length = 0

    for i in range(len(path) - 1):
        (u, v) = (path[i], path[i + 1])
        length += G[u][v][weight]

    return length

def length_of_edge(e, graph, weight='weight'):
    """ Returns the length of a given edge.

    Important: The given edge has to be contained in 'graph'

    Parameters
    ----------
    e: tuple
        An edge in `graph`. Tuple of two nodes.
    graph: nx.DiiGraph
        A weighted directed graph that contains `e`
    weight: str
        The name of the attribute in `graph` speficying the weight/length of an
        edge
    """
    return graph.edges[tail(e), head(e)][weight]

def path_edge_list(path):
    """ Returns the list of edges in a given path represented by a list of nodes
    """
    path_edges = [(path[i], path[i+1]) for i in range(len(path)-1)]

    return path_edges


def reversed_graph(graph: nx.DiGraph):
    """Returns the reversed graph of a given graph.

    The reversed graph consists of the same set of nodes and
    all edges of G, but the directions of the edges are reversed.

    """
    return graph.reverse() #from networkx library


def single_source_shortest_path_tree_T(graph, source, weight='weight'):
    """Creates and returns a shortest path tree.

    The root of the tree is `source` and to every tree node there will be
    exactly one path starting in `source` which is the shortest path in G.

    Parameters
    ----------
    graph: nx.DiGraph
        A directed weighted graph
    source: node
        A node in `graph`. The source for the shortest paths
    weight: str
        The name of the edge attribute which specifies its weight/length

    Returns
    -------
    Tree
        The tree of shortest paths in `G` from `source`

    dict {node: int}
        The nodes of the graph paired with their distance to the source (the
        distance is the sum of the edge weight on the shortest path from the
        source to the node)
    """
    distances_from_source, paths_T = dijkstra.dijkstra_single_source(graph, source, weight)

    # we have got the shortest paths
    #(stored in paths_T as a dictionary of nodes mapped to
    # corresponding node list representing the shortest path).
    # now make a tree T with root source out of it :
    T = Tree(root = source)
    T.paths_from_root = paths_T
    # fill the tree with the node and edge data based on
    # the calculated shortest paths
    for v in graph.nodes:

        # check if node is not the root and if
        # a path exists from source to this node
        if (v != source and paths_T.get(v, False)):
            # add all reachable nodes
            T.add_node(v)

            # add the (unique) edge from which the node v can be reached in T,
            #  so the last edge of the shortest path from t to v

            new_edge_start_node = paths_T[v][-2] #TODO -> path length 1?
            T.add_edge(new_edge_start_node, v,
                   weight=graph.edges[new_edge_start_node, v][weight])

    T.distances_from_root = distances_from_source
    return T, distances_from_source
