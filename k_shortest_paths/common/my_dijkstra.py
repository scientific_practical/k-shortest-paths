import math
import networkx as nx
from networkx import DiGraph

def initialize_distances(G, s):
    """ Creates an empty dictionary for storing the distances of the shortest
    paths from s to every other vertex.

    At first all distances to every node from starting node s are positive
    infinity and no node is visited.
    Distance infinity means therefore that no path starting from s to the
    respective node has been found
    """
    distances = {v: math.inf for v in G.nodes}
    distances[s] = 0
    return distances


def initialize_shortest_paths(G, s):
    """Creates an empty dictionary for storing the shortest paths from s to
    every other vertex.

    At first all paths are the empty path except for node s that can be reached
    visiting only s so the shortest path to s is the path only consisting of
    node s.
    """
    shortest_paths = {v : [] for v in G.nodes()}
    shortest_paths[s].append(s)
    return shortest_paths


def set_current_node(visited, currentDistances):
    """Set the non-visited node with the smallest
    current distance to start node as the current node.
    Returns None if there is no more node to visit.
    """

    # visited is a set that contains the names of the nodes marked as visited. E.g. {'A', 'C'}.
    # currentDistances is a dictionary that contains the current minimum distance of each node. E.g. {'A': 0, 'B': 3, 'C': 5}

    min_distance = math.inf
    new_current_node = None
    for v, dist in currentDistances.items():
        if (dist < min_distance) and (v not in visited) :
            new_current_node = v
            min_distance = dist

    return new_current_node

def update_distances(G: DiGraph, c, visited, shortest_paths, distances, weight="weight"):
    """Examines all successors of the current node c and updates the distances
    of the start node to these sucessors and the corresponding paths (that can
    contain c then)
    """
    for v in G.successors(c):

        # check if c's successors can be reached more quickly passing node c than on
        # the so far known shortest paths there

        if v in visited:
            continue # new path cannot be shorter

        new_distance = distances[c] + G[c][v][weight]
        if (new_distance < distances[v]):
            distances[v] = new_distance
            shortest_paths[v] = shortest_paths[c].copy()
            shortest_paths[v].append(v)


def dijkstra(G: DiGraph, s, weight="weight"):
    """Implementation of single source Dijkstra algorithm for a  directed graph.
    
    Computes the shortest paths in directed graph G from node s to all other
    nodes.

    Parameters
    ----------
    G: nx.DiGraph
        A weighted directed graph
    s: node
        start node
    weight: str
        The name of the attribute of G's edges specifying their weight.

    Returns
    -------
    shortest_paths: `dict` [`node`, `list` [`node`]]
        {vertex_1: [s, ... , vertex_1],..., vertex_n : [s, ... , vertex_n]}
    where [s, ... , vertex_i] is the ordered list of nodes forming the path from 's' to vertex_i
    distances: a dict of the of the format {vertex_1: dist_1, ... vertex_n: dist_n} where dist_i is the
    distance from 's' to vertex_i, i.e. the length of the shortest path between those nodes.

    """

    #initialize variables
    visited = []
    distances = initialize_distances(G, s)
    shortest_paths = initialize_shortest_paths(G, s)


    for i in range(G.number_of_nodes()-1): #last node does not have to be visited
        c = set_current_node(visited, distances)
        if c is None:
            return shortest_paths, distances
        update_distances(G, c, visited, shortest_paths, distances, weight)
        visited.append(c)

    return distances, shortest_paths
