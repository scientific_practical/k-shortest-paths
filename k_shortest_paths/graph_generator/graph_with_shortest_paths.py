import networkx as nx

class GraphWithShortestPaths(nx.DiGraph):
    """Instances of this class are `networkx.DiGraph`s and their coresponding shortest-paths from `s` to `t`.
    The caller has to make sure, that the shortest_paths are actually the shortest_paths.

    The shortest_paths are a list of tuples.
    The tuples contain the path and its length.

    We use instances of this only for unit-testing.
    In our tests we compare `shortest_paths` with our algorithmically computed shortest paths.
    """
    def __init__(self, G: nx.DiGraph = None, s = None, t = None, shortest_simple_paths: [] = []):

        super().__init__(G)
        if (any(not isinstance(p, tuple) for p in shortest_simple_paths)):
            raise RuntimeError("shortest_simple_paths is supposed to be a tuple, combining a path and its length")
        self.shortest_simple_paths = shortest_simple_paths
        self.shortest_paths_from = s
        self.shortest_paths_to = t
