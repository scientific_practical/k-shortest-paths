"""This package contains methods for creating connected DAGs with a corresponding list of shortest-paths between nodes u and v and methods for serializing them as (and deserializing them from) a binary .pickle file.

    References
    ----------
    http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.329.9743&rep=rep1&type=pdf
"""