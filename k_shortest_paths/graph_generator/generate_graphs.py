import networkx as nx
import random

from itertools import islice
from itertools import permutations
from k_shortest_paths.common.digraph_utils import length
from k_shortest_paths.graph_generator.graph_with_shortest_paths import GraphWithShortestPaths

def generate_neighbourhood_graph(n, m) -> nx.DiGraph:
    """Generates a Neighbourhood-Graph with `n*m` nodes and `Theta(n*m)` edges.

    A neighbourhood graph can be viewed as a grid, with `n` rows and `m` nodes in each row.
    Let `v_i,j` be the node in row `i` and column `j`.
    `v_i,j` is connected to `v_i+1,j`, `v_i,j+1`, `v_i+1,j+1`, if `i+1 <= n` and `j+1 <= m`.
    Each edge will be assigned a random integer weight in range [1, 1000].

    The upper-left node has key `1` and the lower-right node has label `n*m`.

    This graph is directed and acyclic, with the additional feature, that the paths
    from the upper left corner to the lower right corner are rather long, compared to random graphs.

    We generate these kinds of graphs for benchmarking our algorithms, as we wanted to examine graphs with long paths.
    """

    G = nx.DiGraph()
    G.add_nodes_from(list(range(1, n*m + 1)))

    # add edges to each node in every row
    for i in range(0, n):
        for j in range(1, m + 1):
            current_node = i * m + j

            # only add edges to the right,
            # if current node is not the rightmost node in the current row.
            if (j < m):
                # node to the right:
                G.add_edge(current_node, current_node + 1, weight=random.randint(1, 1000))

            # only add edges to the bottom of the current node, as long as the current node is not at the bottom
            # of the grid.
            # the bottom of the contains nodes numbered from m * (n-1) + 1, m * (n-1) + 2, ...., + m * (n-1) + m = n*m
            if (current_node <= m * (n-1)):
                # node below:
                G.add_edge(current_node, current_node + m, weight=random.randint(1, 1000))

            # only add edges to the lower-right, if both of the above conditions are met
            if j < m and current_node <= m * (n-1):
                # node diagonally to the lower-right
                G.add_edge(current_node, current_node + 1 + m, weight=random.randint(1, 1000))

    return G


def generate_random_graph(n) -> nx.DiGraph:
    r"""This method generates a random connected DAG `G` with `n` nodes with edges having
    random weights.

    Parameters
    ----------
    n: int
        Number of Nodes the resulting Graph should have.

    Returns
    -------
    DiGraph
        Graph with n nodes, which is guaranteed to be weakly connected and acyclic.
    Notes
    -----
    Here is a short summary of the algorithm:
    Let `p` be a permutation of `V` = {1, ..., `n`}.
    Let `G_0 = (p(V), E_0)`, such that `E_0` is {(p(i), p(i + 1)) | 1 <= i < n}.
    `G_0` is now a connected, directed line-graph.
    For 1 <= `k` <= `m` we iteratively construct a graph `G_k`, by adding and/or removing edges from `G_{k-1}`:
    Let i and j be uniformly random numbers between 1 and n, such that i != j.
    if (p(i), p(j)) \(\in\) \(E_{k-1}\):
        if (p(i), p(j)) is a bridge in `G_{k-1}`, then reverse this edge and let `G_k` = `G_{k-1}` without (p(i), p(j)) and with (p(j), p(i))
        else let `G_k` = `G_{k-1}` without (p(i), p(j))
    else if p(i), p(j)) \(\not\in\) `E_{k-1}`:
        if (p(i), p(j)) closes a circle in E_{k-1}, then do nothing (`G_k`= `G_{k-1}`)
        else (p(i), p(j)) does not close a circle in E_{k-1}, then let `G_k` = `G_{k-1}` with (p(i), p(j))

    References
    ----------
    The algorithm of generating the connected DAG is based on the following paper:
    http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.329.9743&rep=rep1&type=pdf

    """
    # generate digraph with n verticies and m random edges.

    G = nx.DiGraph()
    nodes = list(range(1, n + 1))
    random.shuffle(nodes)
    G.add_nodes_from(nodes)

    # zip(nodes, nodes[1:]) generates list of tuples, like [(1, 2), (2, 3), (3, 4), ...]
    for (i, j) in zip(nodes, nodes[1:]):
        G.add_edge(i, j, weight=random.randint(1, n))

    # we apply the transformation of the initial graph `2*n` times.
    # (there is no definite reason for doing it `2*n` times,
    # it just appeared to be reasonable, so the algorithm can modify the line graph enough
    # such that the resulting graph is more dense and has multiple s-t-paths)
    for m in range(2*n):
        i, j = random.randint(1, n), random.randint(1, n)
        if G.has_edge(i, j):
            attributes = G[i][j]
            G.remove_edge(i, j)

            # is_weakly_connected uses BFS, therefore has complexity O(n+e)
            if (not nx.is_weakly_connected(G)):
                G.add_edge(j, i, **attributes)
        else:
            G.add_edge(i, j, weight=random.randint(1, n))
            # nx.find_cycle uses depth-first traversal, which has complexity of O(e)
            # where n is the amount of nodes, e is the amount of edges
            try:
                # find_cycle throws a NetworkXNoCycle Exception, if no cycle is found.
                nx.find_cycle(G, orientation='original')
                # if (i, j) closed a circle in G, then remove it again.
                G.remove_edge(i, j)
            except:
                # if (i, j) did not close a cycle, keep (i, j) in the graph.
                pass

    return G


def calculate_k_shortest_paths(G: nx.DiGraph, k, desired_source, desired_destination, weight="weight") -> GraphWithShortestPaths:
    """We calculate the k-shortest paths in `G` from `desired_source` to `desired_destination`.

    For calculating `k`-shortest path in `G` we use the networkx-method `shortest_simple_paths()`, which we
    assume to be correct. Let `n` = `len(G.nodes)`
    We normally start the shortest-paths algorithm from node `desired_source` to node `desired_destination`.

    But it may be the case that `desired_source` and `desired_destination` are not connected in `G`.

    In this case, we try `ln`-times to choose a different `desired_destination`. We stop at the first node,
    which is connected to `desired_source`, or if we've tried this process `n` times.
    After that, we fixate the originally proposed `desired_destination` and do the above procedure with choosing a
    different candidate for `desired_source`, also no more than `n` times.

    If this procedure is still not working, we will throw `networkx.NetworkXNoPath`, as any further calculation
    would not be worth the computing power. In that case, the user has to select a better source and destination.

    Parameters
    ----------
    G: DiGraph
        Graph, of which we want to calculate the shortest paths on
    k: int
        number of shortest paths to calculate
    desired_source: node
        a node of G, which resembles the preferred source node for the k shortest paths
    desired_destination: node
        a node of G, which resembles the preferred destination node for the k shortest paths
    weight: string
        (optional) attribue name, which stores the edges' weigths

    Returns
    -------
    GraphWithShortestPaths
        Graph, which contains the calculated k shortest paths.
    """

    # check if there is a path from 1 to n:
    s = desired_source
    t = desired_destination

    node_list = list(G.nodes)

    s_t_connected = False

    for i in range(len(G.nodes)):
        if nx.algorithms.has_path(G, s, t):
            s_t_connected = True
            break

        # there is no path from s to t
        # swap t with a random node of G and try again.

        t = node_list[random.randint(0, len(node_list) - 1)]

        # dont let s and t be the same
        while (t == s):
            t = node_list[random.randint(0, len(node_list) - 1)]

    # if s and t are still not connected, try again, this time switching desired_source
    if (not s_t_connected):
        t = desired_destination
        for i in range(len(G.nodes)):
            if nx.algorithms.has_path(G, s, t):
                s_t_connected = True
                break

        # there is no path from s to t
        # swap t with a random node of G and try again.

        s = node_list[random.randint(0, len(node_list) - 1)]

        # dont let s and t be the same
        while (s == t):
            s = node_list[random.randint(0, len(node_list) - 1)]

    if (not s_t_connected):
        # try again, starting with two random nodes
        s = node_list[random.randint(0, len(node_list) - 1)]
        t = node_list[random.randint(0, len(node_list) - 1)]
        while (s == t):
            t = node_list[random.randint(0, len(node_list) - 1)]
        return calculate_k_shortest_paths(G, k, s, t, weight)

    # get `k`-shortest-simple-paths from known good algorithm:
    simple_paths = list(islice(nx.shortest_simple_paths(G, s, t, weight=weight), k))
    shortest_simple_paths = []
    for path in simple_paths:
        shortest_simple_paths.append((path, length(G, path, weight)))

    # return the test object, so that we can serialize it and use it for our tests.
    G_return = GraphWithShortestPaths(G, s, t, shortest_simple_paths=shortest_simple_paths)
    return G_return
