from networkx import DiGraph
from k_shortest_paths.common.tree import Tree
from networkx import is_directed_acyclic_graph
from networkx.algorithms import single_source_dijkstra
import networkx as nx

class EppsteinTree(Tree):
    """ 
    Instances of this class are connected, acyclic 
    ``networkx.DiGraph``s with a designated vertex `root`.
    In other words, instances of this class are trees 
    with a designated root called `root`.

    An instance is built from a ``networkx.DiGraph`` `G` and a designated
    vertex `root` in `G`. We calculate the single-source-shortest-path in 
    `G` from `root`. The resulting tree is set to be our instance.

    We use this class to construct Heaps ``H_out(v)``, ``H_T(v)`` 
    and ``H_G(v)`` more easily.

    Important: We assume this tree is not root-directed,
    which means every node is directed to its children.

    For convenience and readability, we renamed methods for 
    Tree-specific tasks, e.g. ``DiGraph.successors(v)`` is renamed as
    `get_children(v)` or ``DiGraph.predecessors(v)`` 
    is used in `get_parent(v)` for getting the parent of `v`.

    """

    def __init__(self, graph: DiGraph, root):
        """Instanciates a single-source-shortest-path-tree from source `root`
        
        Raises an exception, if `root` does not exist in `graph`.
        Raises an exception, if `graph` is `None`.
        If `graph` is empty (has no vertices), the value of `root` is ignored and
        set to `None`. 
        Otherwise we calculate the shortest path tree of `graph` from `root`
        and set `root` as the root of the resulting tree.
        """
        if (graph == None):
            raise Exception("Cannot calculate single-source-shortest "
                            "path tree of 'None'")
    
        if (len(graph.nodes) == 0): 
            super().__init__(None)
            return

        super().__init__(root)

        self.__init_as_single_source_shortest_path_tree(graph, root)

    def __init_as_single_source_shortest_path_tree(self, graph: DiGraph, source):
        """Method genereating the shortest path tree from
        the given graph `graph` from a given source vertex `source` and setting 
        edges and nodes of this tree for this instance.
        
        An exception is raised, if `source` is not a vertex of `graph`.
        
        """
        # Exception will be raised in `single_source_dijkstra()`, 
        # if `source` is not in `graph`
        (lengths, paths) = single_source_dijkstra(graph, source)

        # we have got the paths. 
        # now make a tree out of it

        # naive approach adding each path from a vertex `v` to `source` into T
        # nodes are overwritten by DiGraph.add_path and are not duplicated.
        for vertex, path in paths.items(): 
            self.add_path(path)

