"""This module contains an implementation of Eppsteins's k-shortest-paths algorithm.

    References
    ----------
    https://www.ics.uci.edu/~eppstein/pubs/Epp-SJC-98.pdf
"""