"""
This module provides an implementation of Eppstein's k-shortest-paths algorithm
to find k shortest paths between two given nodes in a graph.

Improvement:
References: Lazy Eppstein: https://www.researchgate.net/publication/227141311_A_Lazy_Version_of_Eppstein's_KShortest_Paths_Algorithm
"""
import networkx as nx
import k_shortest_paths.common.digraph_utils as dgu
from k_shortest_paths.common.tree import Tree
import k_shortest_paths.eppstein.eppstein_utils as eu


def eppstein_k_shortest_paths(graph: nx.DiGraph, s, t, k, weight='weight', visualization=False):
    """Computes `k` shortest paths in `G` from vertices `s` to `t`
    using Eppstein's k-shortest-paths algorithm. (See paper).

    Returns a list of tuples containing `k` paths paired with their lengths.
    The first entry of the tuple is the path, the second is the length of the
    path. A path is a list of nodes.

    In Eppstein's Algorithm we compute the k-shortest paths by
    building a lot of crazy heaps and in the end we can map heap nodes
    to s-t-paths in G heap-ordered by their lengths. TODO ^^;

    For details of the single steps, see the implementations in the
    eppstein_utils module TODO insert link

    Parameters
    ----------
    graph: nx.DiiGraph
        Directed weighted graph in which we want to find the
        'k' shortest paths
    s:
        Node in the graph 'graph'. Starting node of the shortest paths.
    t:
        Node in the graph 'graph'. Destination of the shortest paths.
    k: int
        Number of shortest paths we want to find between 's' and 't'
    weight: str
        name of attribute that stores the edge weights in the given graph
    visualization: bool
        if this is true, the method will output a visualization  of all steps
        TODO

    Returns
    -------
    list
        list of 2-tuples containing the `k` shortest paths from `s`to `t`paired
        with their lengths. The first entry of the tuple is the path, the second
        is the length of the path. A path is a list of nodes.

    """

    # --------------------------------------------------------------------------
    # first check Input for validity:
    # s and t must be different
    if s == t:
        return None

    # s and t must be nodes of G
    if not graph.has_node(s) or not graph.has_node(t):
        return None

    # k should be positiv integer
    if k < 1:
        return []
    # --------------------------------------------------------------------------


    eppstein_env = eu.EppsteinEnvironment(graph, s, t , k, weight)

    # --------------------------------------------------------------------------
    # ***  Step 1  ***
    # Create shortest path tree T with destination t
    # --------------------------------------------------------------------------
    # Apply single source Dijkstra on reversed graph G_R with source t
    # and add all nodes and edges on paths to a tree T.

    eppstein_env.create_single_destination_shortest_path_tree_T()


    # --------------------------------------------------------------------------
    # ***  Step 2  ***
    # Build heaps H_out(v) vor all nodes v in G
    # --------------------------------------------------------------------------
    # These heaps contain all sidetracks tailing in v heap-ordered by their
    # delta values. So the root (aka outroot) of H_out(v) represents the
    # sidetrack starting in v which, when taken, results in the shortest detour.

    H_out = eppstein_env.create_all_H_out()


    # --------------------------------------------------------------------------
    # ***  Step 3  ***
    # Build heaps H_T(v) for all v in G.
    # --------------------------------------------------------------------------
    # These heaps contain the outroots of all nodes on the path from
    # v to t in T, heap-ordered by their delta values.

    H_T, marks_of_H_T = eppstein_env.create_all_H_T()


    # --------------------------------------------------------------------------
    # ***  Step 4 and 5  ***
    # Build heaps H_G(v) for all v in G
    # Build D(G)
    # --------------------------------------------------------------------------
    # According to the algorithm in the paper we would create the heaps H_G(v)
    # now for all v in G (4) and then build the graph D(G) (5).
    # However, since these structures are of theoretical interest only, we skip
    # these steps to save time and space. For building the path graph we already
    # have all necessary information.




    # --------------------------------------------------------------------------
    # ***  Step 6  ***
    # Build Path Graph P(G)
    # --------------------------------------------------------------------------
    # For building the path graph we merge all information of all heaps H_out,
    # H_T and our shortest  paths tree T. This structure implicitely contains
    # the information of the possible combination of sidetracks to form an
    # s-t-paths as well as their lengths.
    # Every node in this graph corresponds to a sidetrack in G.
    # A node v in P(G) is a child of another node w in P(G) if v's corresponding
    # sidetrack in G can be added to an s-t-path after or instead of sidetrack
    # corresponding to the parent's node w. We call those edges in P(G)
    # crossedges ("after") or heapedge ("instead").

    P_G = eppstein_env.create_path_graph()


    # --------------------------------------------------------------------------
    # ***  Step 7  ***
    # Build heap H(G)
    # --------------------------------------------------------------------------
    # The heap H(G) consists of all paths in P(G) starting from the root r.
    # Every one of these r-paths corresponds to an s-t-paths in G
    # (and this is a length preserving correspondence).
    # However, we only need the k smallest nodes of this heap. For cyclic graphs
    # `G`, this heap is even infinitely large. So we omit this step too and
    # directly taking the k shortest P_G paths starting from its root r and
    # convert this path to an implicit s-t-path in G (a list of sidetracks).




    # --------------------------------------------------------------------------
    # ***  Step 8  ***
    # Find the implicit representations of the k shortest s-t-paths in G
    # --------------------------------------------------------------------------
    # According to the paper we obtain the implicit representations of the k
    # shortest s-t-paths by extracing the k smallest elements from the heap H_G.
    # As explained in previous step, we do not build the heap H_G.
    # Our function uses a prioritized version of Breadth First Search in P_G
    # starting in r to find the k shortest paths starting in this node.

    k_implicit_paths = eppstein_env.find_k_implicit_shortest_paths_in_P_G()

    # --------------------------------------------------------------------------
    # ***  Step 9  ***
    # Convert the implicit representations to explicit ones
    # --------------------------------------------------------------------------
    # This is the last step of the algorithm. What we get here is a list of
    # the explicit k shortest s-t-paths in increasing order paired with their
    # lengths.

    k_shortest_paths = eppstein_env.create_explicit_from_implicit_paths()


    return k_shortest_paths
