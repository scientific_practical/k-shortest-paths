"""This module contains the actual source code. 

Both algorithms reside in their own packages `k_shortest_paths.hershberger` and `k_shortest_paths.eppstein`.

Methods, which both algorithms rely on (e.g. Dijkstra's algorithm) are found inside `k_shortest_paths.common`.

Methods for analyzing algorithms can be found in `k_shortest_paths.analysis`.

Methods for generating graphs for testing and benchmarking can be found in `k_shortest_paths.graph_generator`.

Methods for visualizing graphs and their calculated paths can be found in `k_shortest_paths.visualization`."""
