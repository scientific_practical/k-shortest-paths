import json

class Statistic():
    """Small class to bundle up statistical values for evaluating the runtime of our algorithms
    
    This class is only used inside this module, hence it's defined inside this module and not in a separate file.
    """
    def __init__(self, k: int, time: int):
        """Represents a Statistical unit of one execution of a ksp-algorithm on one graph, where `k` shortest
        paths could be calculated in `time` nanoseconds.
        """
        self.k = k
        self.time = time