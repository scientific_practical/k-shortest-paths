import sys
import threading
import networkx as nx
from k_shortest_paths.analysis.timer import Timer
from k_shortest_paths.eppstein.eppstein import eppstein_k_shortest_paths
from k_shortest_paths.hershberger.hershberger import hershberger_k_shortest_paths

import json

from k_shortest_paths.graph_generator.graph_with_shortest_paths_repository import load_graph_with_shortest_paths_list
from k_shortest_paths.graph_generator.graph_with_shortest_paths_repository import check_if_serialized_graphs_exist
from k_shortest_paths.graph_generator.graph_with_shortest_paths_repository import save_graph_with_shortest_paths
from k_shortest_paths.graph_generator.graph_with_shortest_paths import GraphWithShortestPaths
from k_shortest_paths.graph_generator.generate_graphs import calculate_k_shortest_paths
from k_shortest_paths.graph_generator.generate_graphs import generate_neighbourhood_graph
from k_shortest_paths.visualization.graph_plot import draw_graph_k_paths

from k_shortest_paths.graph_generator.generate_graphs import generate_random_graph

# prefix of the main-graph name inside the graphs folder
MAIN_GRAPH_NAME_PREFIX = "main"

class KSPThread (threading.Thread):
    """Defines a simple thread instance for calculating k shortest paths with one of the implemented algorithms.

    After calculation has finished, the result can be accessed through the `k_paths` attribute."""
    def __init__(self, alg, graph, args):
        """
        Parameters
        ----------
        alg: function
            A k shortest paths function
        graph: GraphWithShortestPaths
            A graph, which the algorithm should be executed on. `s` and `t` are attributes of `graph`.
        args: tuple
            further arguments, which will be passed to the algorithm `alg`.
        """
        threading.Thread.__init__(self)
        self.alg = alg
        self.args = args
        self.graph = graph
        self.k_paths = []

    def run(self):
        """Runs the alforithm and outputs the time taken to finish it."""
        timer = Timer()

        print("Starting Algorithm {} with arguments (s, t, k)={}...".format(self.alg.__name__, self.args))
        timer.start()
        self.k_paths = self.alg(self.graph, *self.args)
        timer.stop()

        print("Algorithm {} finished in {} nanoseconds.".format(self.alg.__name__ , str(timer.time)))


def main(graph_properties=(20, 20, 100)):
    """By executing the project, we will start to run Eppstein's Algorithm and Hershberger's Algorthim
    in two seperate Threads.
    A neighbourhood-Graph according to `properties` will be generated, if there is no main-graph in the `graphs` folder.

    The graph will then be used by both Algorithms.

    After both Algorithms have finished, the benchmark-results will be shown in the command line.
    Also, a browser-tab will be openend, displaying the graph with the calculated shortest paths.
    """

    (n, m, k) = graph_properties

    if not check_if_serialized_graphs_exist(MAIN_GRAPH_NAME_PREFIX + '*'):
        print("Generating a graph...")
        save_graph_with_shortest_paths(calculate_k_shortest_paths(generate_neighbourhood_graph(n, m), k, 1, n*m), n*m, k, \
            generated_graph_type="neighbourhood", prefix=MAIN_GRAPH_NAME_PREFIX)
        print("Graph generated.")
    main_graphs = load_graph_with_shortest_paths_list(MAIN_GRAPH_NAME_PREFIX + '*')

    # only take one graph, if there are multiple
    G: GraphWithShortestPaths = main_graphs[0]

    # make sure, both algorithms get independent graphs
    # so that they dont interfere with one another
    G1 = G.copy()
    G2 = G.copy()

    ksp_args = (G.shortest_paths_from, G.shortest_paths_to, k)

    eppstein_thread = KSPThread(eppstein_k_shortest_paths, G1, ksp_args)
    hershberger_thread = KSPThread(hershberger_k_shortest_paths, G2, ksp_args)

    eppstein_thread.start()
    hershberger_thread.start()

    eppstein_thread.join()
    hershberger_thread.join()

    print("Drawing the graph with k shortest paths...")
    # take one of the calculated list of paths and output the result.
    draw_graph_k_paths(G, [ path for (path, length) in eppstein_thread.k_paths ])
