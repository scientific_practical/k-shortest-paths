"""This module contains an implementation of Hershberger's k-shortest-paths algorithm.

    References
    ----------
    https://archive.siam.org/meetings/alenex03/Abstracts/jhershberger.pdf
"""