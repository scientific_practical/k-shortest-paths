# Hershberger's k-shortest paths algorithm

## Prerequisites

given:  Graph `G` = (`V`,`E`)

* with `n` vertices and `m` edges
* directed
* weighted, all edges have non-negative lengths
* start `s` \in `V`, destination `t` \in `V`
* `s` and `t` are connected

## Algorithm

1. Initialze:
    1. Initialize "Path-Branching"-Structure `T` with single node `s`.
    2. Compute shortest path from `s` to `t` `path(s, t)`
    3. put `path(s, t)` inside a heap `H`
2. Repeat the following k times:
    1. extract the minimum key `P` from `H`.
    2. If `P` belongs to an equivalence class (abbr.: e.c.) `C(u)` for some node `u`:
        1. Add new edge `(u, t_P)` to `T`
        2. Create new e.c. `C(u, t_P)`
        3. move all Paths `P_t` from `C(u)` \ `P` to `C(u, t_P)` where `P_t` shares at least one edge  with `P` after `u`.
    3. Else (`P` belongs to e.c. `C(u,v)` for some edge `(u,v)`)
        1. Let `w` be the vertex where `P` branches off from `branchPath(u, v)`
        2. In `T`, split `(u, v)` into `(u, w)` and `(w, v)` and add `(w, t_P)`.
        3. Create 4 new e.c. `C(u, w)`, `C(w, v)`, `C(w, t_P)`, `C(w)`
        4. move all the paths `P_{u,v}` of `C(u,v)` \ `P` to their new coresponding e.c.
    4. for each new or modified e.c., compute the shortest path from `s` to `t`, that belongs to the class and add them to the heap. Computing the shortest path is done by solving a "Replacement Path"-Problem
