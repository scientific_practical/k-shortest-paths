import networkx as nx
import heapq
from k_shortest_paths.common.dijkstra import dijkstra_shortest_path
from k_shortest_paths.common.dijkstra import dijkstra_shortest_path_length
from k_shortest_paths.common.tree import Tree
from k_shortest_paths.common.digraph_utils import length
from k_shortest_paths.common.digraph_utils import head
from k_shortest_paths.common.digraph_utils import tail
from k_shortest_paths.common.digraph_utils import Tree
from .path_branch_structure_node import PathBranchStructureNode
from .best_replacement_path import best_replacement_path


def hershberger_k_shortest_paths(G: nx.DiGraph, s, t, K, weight="weight"):
    """Computes `K` shortest paths in `G` from vertices `s` to `t`
    using Hershberger's k-shortest-paths algorithm. (See paper).

    In Hershberger's Algorithm we compute the k-shortest paths by
    constructing the "Path Branch Structure" `T`. `T` is essentially
    a tree with `K` leaves. Each path from the root of `T` to a 
    leave `t_P` directly encodes a shortest path from `s` to `t` in `G`.

    Basically, `T` is constructed iteratively by looking at the `i` previously
    computed shortest paths. Edges (or 'branches') and nodes are
    added to `T`, where the `i+1`th shortest path `P_{i+1}` is 
    braching off of the other `i` previously computed
    shortest paths. Thus, we can keep track of all shortest paths.

    Parameters
    ----------
    G: nx.DiGraph
        Graph, which we compute the shortest s-t-paths on
    s: node
        source-node
    t: node
        target-node
    K: number
        number of shortest paths to compute
    weight: string
        (optional) Name of the edge-attribute containing the edge-weights. (default: 'weight')

    Returns
    -------
    list of tuple
        A list of tuples containing `K` paths paired with their lengths. 
        A path is a list of nodes. The first entry of the tuple is the path, the second is the length of the path.

    
    """

    # first check Input for validity:
    # s and t must be different
    if s == t:
        return None

    # s and t must be nodes of G
    if not G.has_node(s) or not G.has_node(t):
        return None

    # this array of tuples of paths-lengths is returned
    k_shortest_paths = []

    # initialize Path Brach Structure T
    T = Tree(PathBranchStructureNode(s, []))
    shortest_path = dijkstra_shortest_path(G, s, t)

    # heap, containing the shortest path of each equivalence class
    heap:[] = []

    # Note: The heap contains a tuple. heapq sorts by the first value
    # of the tuple (in this case, it's the length of our paths).
    # the second entry is our path.
    # If two items in this heap have the same value, heapq will sort by 
    # the second value of the tuple, and so on.
    heapq.heappush(heap, (length(G, shortest_path, weight), shortest_path))
    temp_length = length(G, shortest_path, weight)

    for k in range(K):
        # if there are no more paths, terminate.
        if len(heap) == 0:
            break

        P_length, P = heapq.heappop(heap)
        # add P to the list of shortest paths
        k_shortest_paths.append((P, P_length))

        # determine which equivalence class P belongs to
        new_classes = determine_equivalence_class_of_path(P, G, T)
        
        # update the heap: for each modified or new equivalence class compute
        # the shortest paths and add them to the heap.
        for new_class in new_classes:
            if type(new_class) is tuple:
                # we have modified/added C(u, v):
                # now we need to get the best replacement path P
                # by replacing path from u up to v.

                # u and v should be an instance of PathBranchStructureNode
                (u, v) = new_class

                if type(u) is not PathBranchStructureNode:
                    raise Exception("Node of Path Branch Structure is " +
                                    "not a PathBranchStructureNode. Type is: {}. Value is {}.".format(type(u), u))
                if type(v) is not PathBranchStructureNode:
                    raise Exception("Node of Path Branch Structure is " +
                                    "not a PathBranchStructureNode. Type is: {}. Value is {}.".format(type(v), v))

                # we define a subgraph H of G
                # where we removed every node of prefix_path(u) and node u itself from G
                # and the path P.
                H = nx.DiGraph(G)
                H.remove_node(u.name)
                H.remove_nodes_from(u.prefix_path)

                # in this graph, we find the best path in the equivalence class of C(u, v)
                # by applying the algorithm for the replacement-path-problem
                # if such a path exists, it is pushed to our heap of paths.

                branch_path_u_v = T[u][v]["branch_path"]
                replacements = [branch_path_u_v[i + 1] for i in range(len(branch_path_u_v) - 1)]
                b = replacements[0]
                try:
                    # here we get the path from node b to t, where b is the head of the first edge in the branch_path.
                    # we need to append this path to prefix_path(b)
                    # best_replacement_path is abbreviated with brp
                    (brp, brp_length) = best_replacement_path(H, replacements, b, t, weight)

                    # concatenate it with prefix_path(b) to get the shortest path in the equivalence class C(u, v)
                    prefix_path = []

                    # b is within prefix_path(v). prefix_path(v) is superset of prefix_path(b)
                    # that's why we can add every node of prefix_path(v) to prefix_path(b) up until we hit b.
                    prefix_path_length = 0
                    for i in range(len(v.prefix_path) - 1):
                        node = v.prefix_path[i]
                        next_node = v.prefix_path[i + 1]
                        if v.prefix_path[i] == b:
                            break
                        prefix_path_length += G[node][next_node][weight]
                        prefix_path.append(node)
                    
                    new_path = prefix_path + brp
                    heapq.heappush(heap, (prefix_path_length + brp_length, new_path))
                except nx.NetworkXNoPath:
                    # if an exception is thrown, there is no path in this equivalence class.
                    pass
            else: 
                # we have modified/added C(u):
                # we compute a new shortest path from u to t

                u = new_class

                # u should be an instance of PathBranchStructureNode
                if type(u) is not PathBranchStructureNode:
                    raise Exception("Node of Path Branch Structure is " +
                                    "not a PathBranchStructureNode. Type is: {}. Value is {}.".format(type(u), u))

                # we define a subgraph H of G
                # where we removed every node of prefix_path(u) from G except for u
                # and for each child v of u in T the first edge of branch_path(u, v)
                H = nx.DiGraph(G)
                H.remove_nodes_from(u.prefix_path[:-1])

                for v in T.get_children(u):
                    try:
                        branch_path_u_v = T[u][v]["branch_path"]
                        (v1, v2) = (u.name, branch_path_u_v[1])
                        H.remove_edge(v1, v2)
                    except nx.NetworkXError:
                        pass

                try:
                    # use dijkstra to compute shortest path from u to t in H
                    shortest_path_in_H = dijkstra_shortest_path(H, u.name, t)

                    # concatenate it with prefix_path(u) to get the shortest path in the equivalence class C(u)
                    new_path = u.prefix_path[:-1] + shortest_path_in_H
                    heapq.heappush(heap, (length(G, new_path, weight), new_path))
                except nx.NetworkXNoPath: 
                    # if an exception is thrown, there is no path in this equivalence class.
                    pass

    return k_shortest_paths
                
            
                
def determine_equivalence_class_of_path(P: [], G: nx.DiGraph, T: nx.DiGraph()):
    """Determines in which equivalence class `P` belongs to and updates the Path-Branch-Structure.
    
    We do this by walking through `P` and checking, whether `P` branches off of
    any branch_path of edges of `T`.

    Returns
    -------
    The updated and new equivalence classes as a list.
    """
    # destination-node:
    t = P[-1]
    t_P = PathBranchStructureNode(t, P)
    new_classes = []

    # Follow the nodes of P to determine, where P branches off of T
    i = 0

    # Last common node between P and T, which is a node in T
    # ((last_common_node, next_common_node) is a branch in T)
    last_common_node = T.root

    # Next common node, which is a node in T, and
    # for which the current node `v` of the path P 
    # (during traversal of P) is between 
    # last_common_node and next_common_node
    # if `v` is a node of T, then next_common_node
    # may be None, because we can't know, which node may be next.
    # ((last_common_node, next_common_node) is a branch in T)
    next_common_node = None

    # denotes the first edge of P, which branched off of previously computed paths.
    branched_off_edge = None

    # traverse through P to determine, in which equivalence class it's in
    while i + 1 < len(P):
        # consider the current edge in P
        current_edge = (P[i], P[i + 1])
        # first determine, which branch the currently 
        # traversed subpath of P is in

        # check, if the current_edge ends in the next_common_node of T
        if next_common_node and tail(current_edge) == next_common_node.name:
        # in the next iteration, the first "if" statement will be true
        # and we look for the next branch in T.
            last_common_node, next_common_node = next_common_node, None

        if tail(current_edge) == last_common_node.name:
            # determine the next direction to go to:
            for child in T.get_children(last_common_node):
                # we check, if current_edge is an edge of prefix_path:

                # check if head of the current_edge is a node of prefix_path
                if head(current_edge) in child.prefix_path:
                    index_of_head = child.prefix_path.index(head(current_edge))
                    # then check if tail of the current_edge preceeds the head in prefix_path
                    # ensuring that the edge is an edge of prefix_path
                    if index_of_head > 0 and child.prefix_path[index_of_head - 1] == tail(current_edge):
                        next_common_node = child
                        break

            # if we have not found any branch from last_common_node
            # to any of its children, then that means, that
            # current_edge is branching off of the 
            # previously computed paths directly at a node of T
            if next_common_node == None:
                branched_off_edge = current_edge
                # we are in an equivalence class of a node
                # create a new branch (last_common_node, t_P)
                # which represents the suffix of P after last_common_node
                
                T.add_node(t_P)
                branch_path = P[i:]
                T.add_edge(last_common_node, t_P, branch_path=branch_path)
                new_classes.append((last_common_node, t_P))
                new_classes.append(last_common_node)
                return new_classes

        # check, if the current edge branches off of any edge inbetween  
        # within branch_path(last_common_node, next_common_node)
        else:
            # tail(current_edge) != last_common_node.name \
            # and (next_common_node and head(current_edge) != next_common_node.name):

            # look for node tail(current_edge) in branch_path(last_common_node, next_common_node)
            # if found, check following node in branch_path(last_common_node, next_common_node)
            # after tail(current_edge). If following node is head(current_edge), then
            # current_edge does not branch off of the previous paths.
            
            # sidenote: we required P to be simple. that's why it's no problem at all
            # to only look at the first appearence of tail(current_edge),
            # because tail(current_edge) may only appear once within our path.
            # if it were to appear twice, there would be a circle in our path,
            # and our path would not be simple anymore.
            branch_path = T[last_common_node][next_common_node]["branch_path"]
            
            # we denote branch_path[w] as tail(current_edge)
            # tail(current_edge) is always on the branch_path. otherwise
            # we would have gone out of the loop beforehand.
            w = branch_path.index(tail(current_edge))
            
            if len(branch_path) < w + 1 or branch_path[w + 1] != head(current_edge):
                # we have found a branched off edge, so we can stop traversing P
                branched_off_edge = current_edge
                # P belongs to an equivalence class C(u, v), because
                # there is a node w (strictly) between u and v,
                # which has an edge (w, z) going to a node z in G. 
                # create new node w and edges (u, w), (w, v) and (w, t_P)
                # and set their branch paths accordingly
                # for readability, consider the following variables w and z like above:
                w = tail(branched_off_edge)
                # z = head(branched_off_edge)
                
                new_prefix_path = P[:P.index(w)] + [w]
                new_node = PathBranchStructureNode(w, new_prefix_path)
                T.add_node(new_node)
                T.add_node(t_P)

                branch_path_u_w = branch_path[:branch_path.index(w)] + [w]
                branch_path_w_v = branch_path[branch_path.index(w):]
                branch_path_w_tP = P[P.index(w):]
                T.add_edge(last_common_node, new_node, branch_path=branch_path_u_w)
                T.add_edge(new_node, next_common_node, branch_path=branch_path_w_v)
                T.add_edge(new_node, t_P, branch_path=branch_path_w_tP)

                T.remove_edge(last_common_node, next_common_node)

                new_classes.append((last_common_node, new_node))
                new_classes.append((new_node, next_common_node))
                new_classes.append((new_node, t_P))
                new_classes.append(new_node)
                return new_classes

        i = i + 1
    raise RuntimeError("Could not determine equivalence class of path {}.".format(P))