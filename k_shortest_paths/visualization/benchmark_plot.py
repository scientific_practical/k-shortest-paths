import glob
from os.path import join
import json
import matplotlib.pyplot as plt
import statistics

STATISTICS_FOLDER_NAME = "statistics"

def find_statistic_files(name="neighbourhood*"):
    """Returns all names of json files containing the collected benchmark data
    for the given graphtype specified in `name`.

    Parameters
    ----------
    name: str
        graphtype + "*". For example "random*" or "neighborhood*"

    Returns
    -------
    list of filenames
        The filenames in the statistics folder matching the pattern
        "benchmark_stats_" + `name` + ".json"
    """
    files = [f for f in glob.glob(join(STATISTICS_FOLDER_NAME, 'benchmark_stats_{}.json'.format(name)))]
    return files


def load_benchmark_statistics(filename):
    """Loads Benchmark-statistics from a JSON-file inside the `statistics`-Folder.

    Parameters
    ----------
    filename: str
        name of the JSON file containing the Benchmark-statistics

    Returns
    -------
    dict:
        The data from the json file in `filename`
    """

    with open(filename, "r") as read_file:
        data = json.load(read_file)

    return data


def plot_neighborhood_statistics():
    """Plots the data from the statistics folder for the neighborhood test
    graphs.
    """

    # --- Load data ---
    neighborhood_files = find_statistic_files("neighbourhood*")
    for file in neighborhood_files:
        neighborhood_data = load_benchmark_statistics(file)
        plot_graph_statistics(neighborhood_data, "Neighborhood graph")


def plot_random_statistics():
    """Plots the data from the statistics folder for the random test graphs.
    """

    # --- Load data ---
    random_files = find_statistic_files("random*")
    for file in random_files:
        random_data = load_benchmark_statistics(file)
        plot_graph_statistics(random_data, "Random graph")


def plot_graph_statistics(graphtype_data, graphtype_str):
    """Creates a plot woth three subplots for the three different node numbers
    of the given data.

    Parameter
    ----------
    graphtype_data:
        Loaded from a json file in statistics folder
    graphtype: str
        type of graph in the benchmark test to the given statistic,
        e.g. "Neighborhood graph" or "Random graph"
    """

    # --- Deserialization ---

    # Read node numbers:
    node_numbers = [int(str_n) for str_n in graphtype_data["eppstein"]]
    node_numbers.sort()
    if len(node_numbers) == 0:
        return

    # Read k's:
    ks = []
    for data_point in graphtype_data["eppstein"][str(node_numbers[0])]:
        if  data_point["k"] not in ks:
            ks.append(data_point["k"])

    # Calculate mean time values and sample standard deviations:
    time_means_eppstein = {}
    errors_eppstein = {}
    time_means_hershberger = {}
    errors_hershberger = {}
    time_means_networkx = {}
    errors_networkx = {}
    for n in node_numbers:
        time_means_eppstein[n], errors_eppstein[n] = calculate_means_in_sec(graphtype_data["eppstein"][str(n)], ks)
        time_means_hershberger[n], errors_hershberger[n] = calculate_means_in_sec(graphtype_data["hershberger"][str(n)], ks)
        time_means_networkx[n], errors_networkx[n] = calculate_means_in_sec(graphtype_data["networkx"][str(n)], ks)


    # --- Configure plots ---

    fig, axs = plt.subplots(nrows=1, ncols=len(node_numbers))
    for i, n in enumerate(node_numbers):
        axs[i].set_title(graphtype_str + "\n" + str(n) + " nodes")

        axs[i].set_xlabel("number of shortest paths $k$")
        axs[i].set_ylabel("time in s")
        axs[i].set_xlim(1,100)

        axs[i].set_xlim(0, max(ks)*1.1)
        axs[i].set_ylim(0, (max(max(time_means_eppstein[n]), max(time_means_hershberger[n]), max(time_means_networkx[n]))+max(errors_hershberger[n]))*1.25)


        FILLSTYLE = 'full'
        axs[i].errorbar(ks, time_means_networkx[n], errors_networkx[n], fmt='s', fillstyle=FILLSTYLE, color='purple', label='Networkx')
        axs[i].errorbar(ks, time_means_eppstein[n], errors_eppstein[n], fmt='o', fillstyle=FILLSTYLE, color='green', label='Eppstein')
        axs[i].errorbar(ks, time_means_hershberger[n], errors_hershberger[n], fmt='^', fillstyle=FILLSTYLE, color='orange', label='Hershberger')
        axs[i].legend(loc="upper left")

        #for logarithmic plot:
        #axs[i].set_yscale('log', basey=10.0)

    fig.set_size_inches(10,5)
    #plt.savefig(filename, format="png", dpi=300)

    plt.tight_layout()
    plt.subplots_adjust(wspace=0.3)
    plt.show()


def calculate_means_in_sec(data_point_list, ks):
    """Calculates the means of the time values in the `data_point_list` for the
    k values in `ks`and converts nanseconds to seconds.

    Parameters
    ----------
    data_point_list: list [ dict{"k": int, "time": int} ]
        list of benchamark statistics for one algorithms and one n.

    ks: list [int]
        list of k's that appear in data_point_list

    Returns
    -------
    dict {k: mean-time}
    """

    # Separate data for different k values:

    data_for_k = {k: [] for k in ks}
    for data_point in data_point_list:
        data_for_k[data_point["k"]].append(data_point["time"])


    # calculate statistical stuff:

    means = []
    sample_standard_deviations = []
    for k in ks:

        mt = statistics.mean(data_for_k[k])
        ssd = statistics.stdev(data_for_k[k])

        # Convert from ns to s
        mt /= 1000000000
        ssd /= 1000000000

        means.append(mt)
        sample_standard_deviations.append(ssd)

    return means, sample_standard_deviations
