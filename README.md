# k-shortest-paths

This project originated during the "scientific computing - practical"-lecture.

## Documentation

We use [pdoc3](https://pdoc3.github.io/pdoc/doc/pdoc/) (install with `pip install pdoc3`) for generating documentation and `numpydoc` as our docstring format.  
See [the scientific-computing documentation lecture](https://gitlab.gwdg.de/scientific_practical/scientific-computing-practical/-/blob/master/lectures/05_documentation.md#example-code) for an example on how to document using the `numpydoc`-format.  

To generate documentation, use the following command:

```["shell"]
pdoc --html --config latex_math=True --force --output-dir doc k_shortest_paths
```

This will create an `doc/k_shortest_paths`-directory, containing the documentation.
You can view the documentation by opening the `index.html`-File inside the `doc/k_shortest_paths` inside your browser.  

We set `latex_math` to `True`, so we can use some Latex expressions in our documentation.  
We set the `--force`-flag, in order to overwrite old documentation inside the `doc` directory.  
We set the `--output-dir` to `doc`, in order to put the documentation inside the `doc` directory (instead of the default `html`-directory).

## Getting started

This project requires several third-party packages.
Install the required packages by running the following command:

```["shell"]
pip install -r requirements.txt
```

## Usage

For a demo, run the program with

```["shell"]
./k_shortest_paths.py
```

For a benchmark-test, run the program with

```["shell"]
./k_shortest_paths_benchmark.py
```

To plot the results of the benchmark test, run:

```["shell"]
./k_shortest_paths_plot_benchmark_results.py
```

To run tests, first create some random-test graphs:

```["shell"]
./k_shortest_paths_generate_test_graphs.py
```

Then run

```["shell"]
pytest
```

## Development

If you want to expand the functionality of this project, feel free to do so.  
Add a new dependency with

```["shell"]
pip install <dependency>
```

*Important*: Make sure to update `requirements.txt` by running the following command:

```["shell"]
pip freeze > requirements.txt
```

## Links

* [Sharelatex](https://sharelatex.gwdg.de/project/5cd5990ba6e84eaea9af10fd)
* [Scientific-Computing Repository](https://gitlab.gwdg.de/scientific_practical/scientific-computing-practical)
* [Old k-shortest-paths](https://gitlab.gwdg.de/scientific_practical/scientific-computing-practical/tree/master/old_projects/lintim_k_shortest_path)
