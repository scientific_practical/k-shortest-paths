#!/usr/bin/env python3

"""This script first generates benchmark graphs, defined in `k_shortest_paths.analysis.benchmark` and runs benchmark tests
against every graph inside the graphs-folder containing "benchmark" in its name.
If there already are benchmark-graphs inside the graphs-folder, the executor may skip the process of generating graphs.

After the benchmarks have finished, the results in json-format will be saved inside a folder called 'statistics'.
"""

from k_shortest_paths.analysis.benchmark import run_benchmark, generate_benchmark_graphs
from k_shortest_paths.graph_generator.graph_with_shortest_paths_repository import check_if_serialized_graphs_exist
from k_shortest_paths.graph_generator.graph_with_shortest_paths_repository import GRAPH_DIRECTORY_NAME

# in case the user has run this script multiple times, we ask him, whether or not they want to generate benchmark-graphs again.
generate_graphs = True

if check_if_serialized_graphs_exist("benchmark*"):
    yesno = None
    while yesno != 'y' and yesno != 'n':
        yesno = str(input("Some benchmark-graphs already exist inside {}-directory.\
            \nContinue generating further benchmark-graphs? (y/n) ".format(GRAPH_DIRECTORY_NAME)))

    if yesno == 'n':
        generate_graphs = False

if generate_graphs:
    generate_benchmark_graphs()

run_benchmark()
