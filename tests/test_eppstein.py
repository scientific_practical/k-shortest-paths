
import networkx as nx
from k_shortest_paths.eppstein.eppstein import eppstein_k_shortest_paths
import pytest
from k_shortest_paths.common.digraph_utils import length
from k_shortest_paths.graph_generator.graph_with_shortest_paths import GraphWithShortestPaths
import k_shortest_paths.visualization.graph_plot as visual
from tests.conftest import get_100_shortest_simple_paths

def test_eppstein_graph_with_one_edge(graph_with_one_edge):
    eppstein, correct = __get_calculated_and_correct_path_lengths(graph_with_one_edge)
    assert eppstein == correct

def test_eppstein_complete_bipartite_graph_k_2_100(complete_bipartite_graph_k_2_100):
    eppstein, correct = __get_calculated_and_correct_path_lengths(complete_bipartite_graph_k_2_100)
    assert eppstein == correct

def test_eppstein_graph_with_non_simple_paths(graph_with_non_simple_paths):
    eppstein, correct = __get_calculated_and_correct_path_lengths(graph_with_non_simple_paths)
    assert eppstein == correct

def test_eppstein_eppstein_example_graph(hershberger_example_graph):
    eppstein, correct = __get_calculated_and_correct_path_lengths(hershberger_example_graph)
    assert eppstein == correct

def test_eppstein_graph_with_two_paths(graph_with_two_paths):
    eppstein, correct = __get_calculated_and_correct_path_lengths(graph_with_two_paths)
    assert eppstein == correct

def test_eppstein_graphs_inside_data_folder(graphs_inside_data_folder):
    for graph in graphs_inside_data_folder:
        k = len(graph.shortest_simple_paths)
        
        # paths, which will be returned by eppstein-algorithm
        eppstein_paths = []
        # pre-calculated paths
        correct_paths = graph.shortest_simple_paths 

        # its possible, that k is not set. If so, correct paths have not yet been 
        # calculated for the current graph, which means, we have to do that now.
        if k == 0:
            k = 100
            correct_paths = get_100_shortest_simple_paths(graph, graph.shortest_paths_from, graph.shortest_paths_to)
        
        eppstein_paths = eppstein_k_shortest_paths(graph, graph.shortest_paths_from, graph.shortest_paths_to, k)
        for i in range(min(k, len(correct_paths))):
            assert eppstein_paths[i][1] == correct_paths[i][1]



def __get_calculated_and_correct_path_lengths(G: GraphWithShortestPaths):
    """Helper Method for testing. Eppstein is called within this method. 
    Both the lengths of the correct paths and lengths of calculated paths are extracted and returned, so that only the lengths
    of the paths are compared.

    We do this, because there could be multiple shortest s-t paths which are different, but have same lengths. The order of the paths
    is not definied, and thus, could lead to a test-failure if the paths were to be compared to eachother.

    Parameters
    ----------
    G: GraphWithShortestPaths
        The Graph, which Eppstein should be tested on.

    Returns
    -------
    tuple of lists
        First element of the tuple are a list of the lengths of the calculated paths. 
        The second element is a list of the lengths of the correct paths.
    """
    eppstein_paths = eppstein_k_shortest_paths(G, G.shortest_paths_from, G.shortest_paths_to, len(G.shortest_simple_paths))
    eppstein_lengths = [l for (p, l) in eppstein_paths]
    correct_lengths = [l for (p, l) in G.shortest_simple_paths]

    return (eppstein_lengths, correct_lengths)